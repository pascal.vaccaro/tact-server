# Server TACT

> Node.js / express server to handle video uploads and downloads for the web browser

## Requirements
- Node.js >= 14
- NPM > 7

## Installation

In the project root, run the following command:

```
npm install
```

## Start

To run the web server on the port 5000, run the following command:

```
npm start
```

If you need to start the web server on a different port, use an env variable:

```
// Unix/MacOS
PORT=<YOUR_PORT_NUMBER> npm start

// Windows
$env:PORT=<YOUR_PORT_NUMBER> npm start
```

## Troubleshooting

1. Writing/reading a video throws an error

This might be because the `uploads` folder is not on the file system, or its access is not properly set.
The `uploads` folder needs to be located at the project root, and it needs read/write access (`0o666`).

2. Server won't start (node.js crashes without launching the web server)

This might be because the `node_modules` folder is not on the file system, or maybe the dependencies were not correctly installed.
Try removing the `node_modules` folder and the `package-lock.json` file, and run `npm install` once again before relaunching the server with `npm start`. 




