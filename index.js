const { join, resolve } = require("path");
const { promisify } = require("util");
const { Transform } = require("stream");
const fs = require("fs");
const express = require("express");
const cors = require("cors");
const ffmpeg = require("fluent-ffmpeg");

const port = process.env.PORT || 5000;
const publicFolder = resolve(__dirname, "public");
const uploadFolder = resolve(__dirname, "uploads");
const indexFile = join(publicFolder, "video.html");
const notFoundFile = join(publicFolder, "404.html");

const mkdir = promisify(fs.mkdir);
const access = promisify(fs.access);
const removeFile = promisify(fs.unlink);
access(uploadFolder, fs.constants.W_OK).catch(() => mkdir(uploadFolder, { mode: 0o666 }));

const getWavFilePath = id => join(uploadFolder, `audio-${id}.wav`);
const getMp3FilePath = id => join(uploadFolder, `audio-${id}.mp3`);
const getImageFilePath = id => join(uploadFolder, `image-${id}.png`);
const getVideoFilePath = id => join(uploadFolder, `TACTcreation_IRCAM-POMPIDOU_${id}.webm`);

express()
  .use("/uploads", express.static(uploadFolder))
  .use(express.static(publicFolder))
  .use(cors())
  .use(express.raw({ limit: '50mb' }))
  .use(express.urlencoded({ limit: '50mb', extended: false }))

  .get("/video/:id", async (req, res) => {
    const { id } = req.params;
    const filePath = getVideoFilePath(id);
    res.setHeader("Content-Type", "text/html");
    try {
      await access(filePath, fs.constants.R_OK);
    } catch (e) {
      const imageFile = getImageFilePath(id);
      const audioFile = getMp3FilePath(id);

      Promise.all([access(audioFile, fs.constants.R_OK), access(imageFile, fs.constants.R_OK)])
        .then(() => ffmpeg(imageFile)
          .size("50%")
          .loop(1)
          .fps(10)
          .duration(15)
          .addInput(audioFile)
          .save(filePath)
          .on("end", () => [audioFile, imageFile].forEach(file => removeFile(file)))
        )
        .catch(() => undefined);

      return res.status(404).sendFile(notFoundFile);
    }

    fs.createReadStream(indexFile)
      .pipe(new Transform({ transform: (chunk, _, cb) => {
        cb(null, Buffer.from(chunk.toString().replace("{{ id }}", id)));
      }}))
      .pipe(res);
  })

  .post("/image/:id", (req, res) => {
    req.pipe(fs.createWriteStream(getImageFilePath(req.params.id), { flags: "a+", mode: 0o666 }), { end: true })
      .on("error", (err) => res.status(400).send(err.message))
      .on("finish", () => res.status(201).send());
  })
  .post("/audio/:id", (req, res) => {
    const wavFile = getWavFilePath(req.params.id);
    req.pipe(fs.createWriteStream(wavFile, { flags: "a+", mode: 0o666 }), { end: true })
      .on("error", (err) => res.status(400).send(err.message))
      .on("finish", () => {
        res.status(201).send();
        ffmpeg(wavFile).save(getMp3FilePath(id)).on("end", () => removeFile(wavFile));
      });
  })

  .listen(port, () => console.log("Server running on port", port));